package main

import (
    "database/sql"
    "encoding/json"
    "fmt"
    "log"
    "net/http"
	"github.com/rs/cors"


    "github.com/gorilla/mux"
    _ "github.com/go-sql-driver/mysql"
)

type Data struct {
    ID          int    `json:"id"`
    Email    	string `json:"email"`
    FirstName   string `json:"firstName"`
    MiddleName  string `json:"middleName"`
    LastName    string `json:"lastName"`
}

func main() {
    username := "avnadmin"
    password := "AVNS_gAPUcl358bbtvxU7Ain"
    host := "cogsystem-cognaic.h.aivencloud.com"
    port := "18444"
    databaseName := "cogsystem"

    dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", username, password, host, port, databaseName)

    db, err := sql.Open("mysql", dsn)
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    // Check if the connection is successful
    err = db.Ping()
    if err != nil {
        log.Fatal("Error connecting to the database: ", err)
    }
    fmt.Println("Connected to the database!")

    r := mux.NewRouter()

    r.HandleFunc("/data", func(w http.ResponseWriter, r *http.Request) {
        w.Header().Set("Content-Type", "application/json")

        // Query data from your database
        rows, err := db.Query("SELECT memberId, email, firstName, middleName, lastName FROM member")
        if err != nil {
            http.Error(w, "Error querying data from the database", http.StatusInternalServerError)
            log.Println("Error querying data from the database: ", err)
            return
        }
        defer rows.Close()

        var data []Data
        for rows.Next() {
            var member Data
            if err := rows.Scan(&member.ID, &member.Email, &member.FirstName, &member.MiddleName, &member.LastName); err != nil {
                log.Println("Error scanning row: ", err)
                continue
            }
            data = append(data, member)
        }

        // Encode the queried data to JSON and write it to the response
        if err := json.NewEncoder(w).Encode(data); err != nil {
            http.Error(w, "Error encoding data to JSON", http.StatusInternalServerError)
            log.Println("Error encoding data to JSON: ", err)
            return
        }
    })

    // Start the server
    log.Println("Server running on :4000")
	handler := cors.Default().Handler(r)
	log.Fatal(http.ListenAndServe(":4000", handler))
}



	////
/*
	type Schema1 struct {
		ID        int
		Name      string
		FirstName string
		LastName  string
	}
	id := 325

	// Query data from schema1 (Borrower)
	var borrower Schema1
	err = db.QueryRow("SELECT memberId, firstName, middleName, lastName FROM member WHERE memberId = ?", id).Scan(&borrower.ID, &borrower.Name, &borrower.FirstName, &borrower.LastName)
	if err != nil {
		log.Println("Error fetching borrower:", err)
	} else {
		fmt.Println("Borrower:", borrower)
	}

	email := "julie.rimas@gmail.com"
	firstName := "John"
	middleName := "Doe"
	lastName := "Smith"
	age := 35
	sex := "female"

	_, err = db.Exec("INSERT INTO member ( email, firstName, middleName, lastName, age, sex) VALUES (?, ?, ?, ?, ?, ?)", email, firstName, middleName, lastName, age, sex)
	if err != nil {
		log.Fatal("Error inserting member:", err)
	}

	fmt.Println("Member added successfully!")

	///

}
*/